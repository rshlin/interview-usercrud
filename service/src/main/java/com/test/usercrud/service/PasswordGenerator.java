package com.test.usercrud.service;

/**
 * Created by ruslan on 7/16/17.
 */
public interface PasswordGenerator {
    String generate();
}
