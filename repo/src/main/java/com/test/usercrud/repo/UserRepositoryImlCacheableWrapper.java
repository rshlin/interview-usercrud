package com.test.usercrud.repo;

import com.test.usercrud.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by ruslan on 7/16/17.
 */
@Primary
@Repository
public class UserRepositoryImlCacheableWrapper implements UserRepository {

    private static final String USER_CACHE = "userCache";
    private static final String USERS_BY_ROLE_CACHE = "usersByRoleCache";

    @Qualifier("userRepositoryImpl")
    private UserRepository userRepository;

    @Autowired
    public UserRepositoryImlCacheableWrapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Cacheable(value = USER_CACHE, key = "{#email, 'email'}")
    public UserEntity findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Cacheable(value = USERS_BY_ROLE_CACHE, key = "#role")
    public Collection<UserEntity> findByRole(String role) {
        return userRepository.findByRole(role);
    }

    @Caching(
            put = {
                    @CachePut(value = USER_CACHE, key = "{#result.id, 'id'}", unless = "#result == null"),
                    @CachePut(value = USER_CACHE, key = "{#result.email, 'email'}", unless = "#result == null")
            },
            evict = {
                    @CacheEvict(value = USERS_BY_ROLE_CACHE, allEntries = true)
            }
    )
    @Transactional
    public UserEntity save(UserEntity userEntity) {
        return userRepository.save(userEntity);
    }

    @Cacheable(value = USER_CACHE, key = "{#id, 'id'}", unless = "#result == null")
    public UserEntity findOne(Serializable id) {
        return userRepository.findOne(id);
    }

    public boolean exist(Serializable id) {
        return userRepository.exist(id);
    }

    public long count() {
        return userRepository.count();
    }

    @Caching(
            evict = {
                    @CacheEvict(value = USER_CACHE, allEntries = true),
                    @CacheEvict(value = USERS_BY_ROLE_CACHE, allEntries = true)
            }
    )
    @Transactional
    public void delete(Serializable id) {
        userRepository.delete(id);
    }

    @Caching(
            evict = {
                    @CacheEvict(value = USER_CACHE, key = "{#userEntity.id, 'id'}"),
                    @CacheEvict(value = USER_CACHE, key = "{#userEntity.email, 'email'}"),
                    @CacheEvict(value = USERS_BY_ROLE_CACHE)
            }
    )
    public void delete(UserEntity userEntity) {
        userRepository.delete(userEntity);
    }
}
