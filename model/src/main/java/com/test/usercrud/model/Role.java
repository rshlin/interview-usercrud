package com.test.usercrud.model;

/**
 * Created by ruslan on 7/12/17.
 */
public enum Role {
    ADMIN,
    DEVELOPER,
    USER
}
