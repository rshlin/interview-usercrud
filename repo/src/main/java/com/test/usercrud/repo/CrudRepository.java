package com.test.usercrud.repo;

import com.test.usercrud.model.PersistentObject;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * Created by ruslan on 7/12/17.
 */
@Transactional(readOnly = true)
public interface CrudRepository<E extends PersistentObject> {

    @Transactional
    E save(E e);

    E findOne(Serializable id);

    boolean exist(Serializable id);

    long count();

    @Transactional
    void delete(Serializable id);

    default void delete(E e) {
        delete(e.getId());
    }
}
