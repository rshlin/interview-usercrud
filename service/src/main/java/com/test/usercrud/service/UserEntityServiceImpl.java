package com.test.usercrud.service;

import com.test.usercrud.model.UserEntity;
import com.test.usercrud.repo.UserRepository;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.Collection;

/**
 * Created by ruslan on 7/14/17.
 */
@Service
public class UserEntityServiceImpl implements UserEntityService {

    private static final String USER_NOT_FOUND_MSG = "user is not present in repository";

    private UserRepository userRepository;

    private PasswordGenerator passwordGenerator;

    @Autowired
    public UserEntityServiceImpl(UserRepository userRepository, PasswordGenerator passwordGenerator) {
        this.userRepository = userRepository;
        this.passwordGenerator = passwordGenerator;
    }

    @Override
    public UserEntity create(UserEntity user) {
        Assert.isNull(user.getId(), "id must be null when creating");
        if (StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(passwordGenerator.generate());
        }
        return userRepository.save(user);
    }

    @Override
    public UserEntity update(UserEntity user) {
        Assert.notNull(user.getId(), "id must not be null when updating");
        Assert.isTrue(userRepository.exist(user.getId()), USER_NOT_FOUND_MSG);
        return userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        Assert.isTrue(userRepository.exist(id), USER_NOT_FOUND_MSG);
        userRepository.delete(id);
    }

    @Override
    public UserEntity get(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public UserEntity getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Collection<UserEntity> getByRole(@NotEmpty String role) {
        return userRepository.findByRole(role);
    }
}
