INSERT INTO users (first_name, last_name, email, password)
VALUES ('userName', 'userLastName', 'user@is.me', 'user');

INSERT INTO users (first_name, last_name, email, password)
VALUES ('devName', 'devLastName', 'dev@is.me', 'dev');

INSERT INTO users (first_name, last_name, email, password)
VALUES ('adminName', 'adminLastName', 'admin@is.me', 'admin');

INSERT INTO user_roles (role, user_id) VALUES
  --   user
  ('USER', 100000),
  --   developer
  ('DEVELOPER', 100001),
  ('USER', 100001),
  --   admin
  ('DEVELOPER', 100002),
  ('USER', 100002),
  ('ADMIN', 100002);
