#CRUD REST сервис для интервью
Простой рест сервис для менеджемента пользователей. Описание ТЗ в файле TASK.md
##Особенности реализации
* Stateless Base64 аутентификация
* Отсутствуют ДТО моделей в web слое приложения
* Отсутствуют business exceptions
##Подготовка к запуску
1) Установить JDK версии 8 и выше
2) Установить postgres
3) Создать бд и пользователя бд c паролем согласно параметрам spring.datasource.* в файле $projectDir/src/main/resources/application.properties
    ```postgresql
    CREATE USER test WITH PASSWORD 'test';
    CREATE DATABASE usercrud WITH OWNER test;
    GRANT ALL PRIVILEGES ON DATABASE usercrud TO test;
    ```
    
##Запуск приложения
* администратор по-умолчанию - login: admin@is.me password: admin 
* Приложение по-умолчанию слушает порт 8080
#### Jar
```bash
./gradlew build && java -jar build/libs/usercrud-0.0.1-SNAPSHOT.jar
```
#### Gradle
```bash
./gradlew bootRun
```
##REST API
* Добавление пользователя
  ```
  POST /rest/users
  body: {}
  ```
  Пример
  ```bash
  curl -H "Content-Type: application/json" --user admin@is.me:admin -X POST localhost:8080/rest/users/ -d '{"email":"newuser@is.me","firstName":"newuserName","lastName":"newuserLastName","password":"123", "roles":["USER"]}'
  ```
* Получение данных пользователя
  - По id
    ```
    GET /rest/users/${id}
    ```
    Пример
    ```bash
    curl --user admin@is.me:admin localhost:8080/rest/users/100002
    ```
  - По логину
    ```
    GET /rest/users/?email=${email}
    ```
    Пример
    ```bash
    curl --user admin@is.me:admin 'localhost:8080/rest/users/?email=dev@is.me'
    ```
  - По роли
    ```
    GET /rest/users/?role=${role}
    ```
    Пример
    ```bash
    curl --user admin@is.me:admin 'localhost:8080/rest/users/?role=ADMIN'
    ```
* Редактирование пользователя
  ```
  PUT /rest/users/${id}
  body: {}
  ```
  Пример
  ```bash
  curl -H "Content-Type: application/json" --user admin@is.me:admin -X PUT localhost:8080/rest/users/100001 -d '{"email":"dev@is.me","firstName":"devName","lastName":"devLastName","password":"123","roles":["DEVELOPER","USER","ADMIN"]}'
  ```
* Удаление пользователя
  ```
  DELETE /rest/users/${id}
  ```
  Пример
  ```bash
  curl --user admin@is.me:admin -X DELETE localhost:8080/rest/users/100003
  ```
