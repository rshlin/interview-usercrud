package com.test.usercrud.repo;

import com.test.usercrud.model.UserEntity;

import java.util.Collection;

/**
 * Created by ruslan on 7/12/17.
 */
//TODO cached wrapper
public interface UserRepository extends CrudRepository<UserEntity> {

    UserEntity findByEmail(String email);

    Collection<UserEntity> findByRole(String role);
}
