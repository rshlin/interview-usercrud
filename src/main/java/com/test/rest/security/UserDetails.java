package com.test.rest.security;

import com.test.usercrud.model.Role;
import com.test.usercrud.model.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by ruslan on 7/14/17.
 */
public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

    private UserEntity userEntity;

    private Collection<GrantedAuthority> authorities;

    public UserDetails(UserEntity userEntity) {
        this.userEntity = userEntity;
        Set<Role> roles = Optional.ofNullable(userEntity.getRoles()).orElseGet(Collections::emptySet);
        this.authorities = roles.stream()
                .map(role ->
                        new SimpleGrantedAuthority(role.name())
                )
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return userEntity.getPassword();
    }

    @Override
    public String getUsername() {
        return userEntity.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
