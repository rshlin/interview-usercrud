package com.test.usercrud.service;

import com.test.usercrud.model.UserEntity;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Created by ruslan on 7/12/17.
 */
public interface UserEntityService {

    @Transactional
    UserEntity create(@NotNull UserEntity user);

    @Transactional
    UserEntity update(@NotNull UserEntity user);

    @Transactional
    void delete(@NotNull Long id);

    UserEntity get(@NotNull Long id);

    UserEntity getByEmail(@NotEmpty String email);

    Collection<UserEntity> getByRole(@NotEmpty String role);
}
