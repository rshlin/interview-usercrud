DROP TABLE IF EXISTS user_roles;
DROP TABLE IF EXISTS users;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START 100000;

CREATE TABLE users
(
  id         BIGINT PRIMARY KEY DEFAULT nextval('global_seq'),
  first_name VARCHAR NOT NULL,
  last_name  VARCHAR NOT NULL,
  patronymic VARCHAR,
  email      VARCHAR NOT NULL UNIQUE,
  password   VARCHAR NOT NULL
);

CREATE TABLE user_roles
(
  user_id BIGINT NOT NULL,
  role    VARCHAR,
  CONSTRAINT user_roles_idx UNIQUE (user_id, role),
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
