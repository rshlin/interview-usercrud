package com.test.rest.controller;

import com.test.usercrud.model.UserEntity;
import com.test.usercrud.service.UserEntityService;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;

/**
 * Created by ruslan on 7/12/17.
 */
@RestController
@RequestMapping(value = UserController.REST_URL)
public class UserController {

    static final String REST_URL = "/rest/users";

    private UserEntityService userEntityService;

    @Autowired
    public UserController(UserEntityService userEntityService) {
        this.userEntityService = userEntityService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserEntity get(@PathVariable long id) {
        return userEntityService.get(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserEntity> create(@RequestBody @Valid UserEntity user) {
        user.setId(null);
        UserEntity created = userEntityService.create(user);

        URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(REST_URL + "/{id}")
                .buildAndExpand(created.getId()).toUri();

        return ResponseEntity.created(uriOfNewResource).body(created);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long id) {
        userEntityService.delete(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody @Valid UserEntity user, @PathVariable long id) {
        user.setId(id);
        userEntityService.update(user);
    }

    @RequestMapping(method = RequestMethod.GET, params = "email", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserEntity getByMail(@RequestParam @NotEmpty String email) {
        return userEntityService.getByEmail(email);
    }

    @RequestMapping(method = RequestMethod.GET, params = "role", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<UserEntity> getByRole(@RequestParam @NotEmpty String role) {
        return userEntityService.getByRole(role);
    }
}
