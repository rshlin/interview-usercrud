package com.test.usercrud.service;

import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by ruslan on 7/16/17.
 */
@Component
public class SecureRandomPasswordGeneratorImpl implements PasswordGenerator {

    private SecureRandom random = new SecureRandom();

    @Override
    public String generate() {
        return new BigInteger(70, random).toString(32);
    }
}
