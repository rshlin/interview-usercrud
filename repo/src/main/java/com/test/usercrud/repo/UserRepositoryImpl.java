package com.test.usercrud.repo;

import com.test.usercrud.model.Role;
import com.test.usercrud.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Created by ruslan on 7/12/17.
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final BeanPropertyRowMapper<UserEntity> ROW_MAPPER = BeanPropertyRowMapper.newInstance(UserEntity.class);
    private static final RowMapper<Role> ROLE_ROW_MAPPER = (rs, rowNum) -> Role.valueOf(rs.getString("role"));

    private final SimpleJdbcInsert simpleInsert;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserRepositoryImpl(DataSource dataSource, NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.simpleInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("users")
                .usingGeneratedKeyColumns("id");
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;

    }


    @Override
    public UserEntity findByEmail(String email) {
        return setRoles(jdbcTemplate.queryForObject("SELECT * FROM users WHERE email=?", ROW_MAPPER, email));
    }

    @Override
    public Collection<UserEntity> findByRole(String role) {
        return jdbcTemplate.query("SELECT * FROM users WHERE id IN " +
                        "(SELECT user_id FROM user_roles WHERE role=?) " +
                        "ORDER BY id ASC",
                ROW_MAPPER, role)
                .stream()
                .map(this::setRoles)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public UserEntity save(UserEntity userEntity) {
        MapSqlParameterSource map = getMapSqlParameterSource(userEntity);

        if (userEntity.isNew()) {
            create(userEntity, map);
        } else {
            update(userEntity, map);
        }
        return userEntity;
    }

    @Override
    public UserEntity findOne(Serializable id) {
        List<UserEntity> users = jdbcTemplate.query("SELECT * FROM users WHERE id=?", ROW_MAPPER, id);
        return setRoles(DataAccessUtils.singleResult(users));
    }

    @Override
    public boolean exist(Serializable id) {
        return nonNull(findOne(id));
    }

    @Override
    public long count() {
        return DataAccessUtils.singleResult(
                jdbcTemplate.query("SELECT count(*) FROM users", (rs, rowNum) -> rs.getLong(0))
        );
    }

    @Override
    public void delete(Serializable id) {
        jdbcTemplate.update("DELETE FROM users WHERE id=?", id);
    }

    private MapSqlParameterSource getMapSqlParameterSource(UserEntity userEntity) {
        return new MapSqlParameterSource()
                .addValue("id", userEntity.getId())
                .addValue("first_name", userEntity.getFirstName())
                .addValue("last_name", userEntity.getLastName())
                .addValue("patronymic", userEntity.getPatronymic())
                .addValue("email", userEntity.getEmail())
                .addValue("password", userEntity.getPassword());
    }

    private void update(UserEntity userEntity, MapSqlParameterSource map) {
        deleteRoles(userEntity);
        insertRoles(userEntity);
        namedParameterJdbcTemplate.update(
                "UPDATE users SET " +
                        "first_name=:first_name, " +
                        "last_name=:last_name, " +
                        "patronymic=:patronymic " +
                        "WHERE id=:id", map);
        if (!StringUtils.isEmpty(userEntity.getPassword())) {
            namedParameterJdbcTemplate.update(
                    "UPDATE users SET " +
                            "password=:password " +
                            "WHERE id=:id", map);
        }
    }

    private void create(UserEntity userEntity, MapSqlParameterSource map) {
        Number newKey = simpleInsert.executeAndReturnKey(map);
        userEntity.setId(newKey.longValue());
        insertRoles(userEntity);
    }


    private void insertRoles(UserEntity userEntity) {
        Set<Role> roles = userEntity.getRoles();
        if (Objects.isNull(roles))
            return;
        Iterator<Role> iterator = roles.iterator();

        jdbcTemplate.batchUpdate("INSERT INTO user_roles (user_id, role) VALUES (?, ?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setLong(1, userEntity.getId());
                        ps.setString(2, iterator.next().name());
                    }

                    @Override
                    public int getBatchSize() {
                        return roles.size();
                    }
                });
    }

    private void deleteRoles(UserEntity userEntity) {
        jdbcTemplate.update("DELETE FROM user_roles WHERE user_id=?", userEntity.getId());
    }

    private UserEntity setRoles(UserEntity user) {
        if (user != null) {
            List<Role> roles = jdbcTemplate.query("SELECT role FROM user_roles  WHERE user_id=?",
                    ROLE_ROW_MAPPER, user.getId());
            user.setRoles(roles);
        }
        return user;
    }
}
