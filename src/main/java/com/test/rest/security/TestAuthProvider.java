package com.test.rest.security;

import com.test.usercrud.model.UserEntity;
import com.test.usercrud.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by ruslan on 7/14/17.
 */
@Component
public class TestAuthProvider extends AbstractUserDetailsAuthenticationProvider {

    @Qualifier("UserEntityServiceImpl")
    private UserEntityService userEntityService;

    @Autowired
    public TestAuthProvider(UserEntityService userEntityService) {
        this.userEntityService = userEntityService;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        boolean validCredentials = authentication.getCredentials().equals(userDetails.getPassword());
        if (!validCredentials) {
            throw new BadCredentialsException("login/password don't match");
        }
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        UserEntity found = userEntityService.getByEmail(username);
        if (Objects.isNull(found)) {
            throw new AuthenticationException("user with specified email is not found") {
            };
        }

        return new com.test.rest.security.UserDetails(found);
    }
}
